import React,{useState} from 'react';
import "./SelectProject.css";


function SelectProjectModal({setOpenModal1}) {
    const [loading] = useState(false);
    
    const DisplayProjects = [
        {
            projec_tName: "Social Media Graphics",
            details: "#098 #Prakria Direct #Digital",
            last_update: "02 March, 17:00",
            file_size: "300 MB"
        },
        {
            projec_tName: "Website Homepage",
            details: "#098 #Prakria Direct #Digital",
            last_update: "02 March, 17:00",
            file_size: "300 MB"
        }]
        if (loading){
            // return <h4>Loading Project Data...</h4>
        }
        else{
            var project_list="";
            project_list= DisplayProjects.map((val, key) => {
                return(
                    <tr key={key} onClick={() => {
                        setOpenModal1(false);
                      }}>
                    
                     <td>
                         <div className='project-down-arrow'><img src={require('./Assets/drop-down.png')} alt=''/></div></td>
                     <td>
                         <div className='project-name'>{val.projec_tName}</div></td>
                     {/* <td>{val.details}</td> */}
                     <div className='project-details'>
                     <td>{val.last_update}</td>
                     <td>{val.file_size}</td>
                     </div>
                    </tr>

                );
            });
        }
        
    return (
        <div className="projectmodalBackground">
        <div className="projectmodalContainer">
        <h2>Select a Project</h2>
          <div className="titleCloseBtn">
          {/* <button
            onClick={() => {
              setOpenModal1(false);
            }}
          >
            X
          </button> */}
        </div>
        <table className='project-list'>
                                    <thead>
                                        <tr>      
                                            <th></th>                                   
                                            <th>PROJECT NAME</th>
                                            <th>LAST UPDATE</th>
                                            <th>FILE SIZE</th>
                                           
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {project_list}
                                    </tbody>
                                </table>
        
        </div>
        </div>
    );
}
        


export default SelectProjectModal;