import React, { useState } from "react";
import "./Modal.css";
import SelectProjectModal from "./select a project/SelectProjectModal";

function Modal({ setOpenModal }) {
  const[modalOpen1,setModalOpen1]=useState(false);
  const plans=[
    {
      price:"$0.00",
      days:"7 Days",
    },
    {
      price:"$0.00",
      days:"14 Days",
    },
    {
      price:"$0.00",
      days:"30 Days",
    }
  ]
  return (
    <div className="modalBackground">
      <div className="modalContainer">
        <div className="titleCloseBtn">
          <button
            onClick={() => {
              setOpenModal(false);
            }}
          >
            X
          </button>
        </div>
        <ul className="plan-list">
                {plans.map((val, key) => {
                    return (
                        <li
                            key={key}
                            // id={window.location.pathname == val.link ? "active" : ""}
                            className="row"
                            
                            // onClick={() => {
                            //     window.location.pathname = val.link
                            // }}
                        ><div className="divider"></div>
                        
                            <div>
                                {val.price}

                            </div>
                            <div>
                                {val.days}
                            </div>
                            <div>
                            
                                <button className="add-to-button" onClick={() => {
                                            setModalOpen1(true);
                                        }}>Add to plan</button>
                            </div>
                            
                            
                        </li>
                    )
                })}
            </ul>
            
        {/* <div className="title">
          <h1>Are You Sure You Want to Continue?</h1>
        </div>
        <div className="body">
          <p>The next page looks amazing. Hope you want to go there!</p>
        </div>
        <div className="footer">
          <button
            onClick={() => {
              setOpenModal(false);
            }}
            id="cancelBtn"
          >
            Cancel
          </button>
          <button>Continue</button>
        </div> */}
      </div>
      {modalOpen1 && <SelectProjectModal setOpenModal1={setModalOpen1} />}
    </div>
  );
}

export default Modal;